<libosinfo version="0.0.1">
<!-- Licensed under the GNU General Public License version 2 or later.
     See http://www.gnu.org/licenses/ for a copy of the license text -->
  <os id="http://opensuse.org/opensuse/tumbleweed">
    <short-id>opensusetumbleweed</short-id>
    <_name>openSUSE Tumbleweed</_name>
    <version>tumbleweed</version>
    <_vendor>openSUSE</_vendor>
    <family>linux</family>
    <distro>opensuse</distro>
    <upgrades id="http://opensuse.org/opensuse/42.3"/>
    <derives-from id="http://opensuse.org/opensuse/42.3"/>

    <variant id="dvd">
      <_name>openSUSE Tumbleweed (DVD Image)</_name>
    </variant>
    <variant id="network">
      <_name>openSUSE Tumbleweed (Network Image)</_name>
    </variant>
    <variant id="kubic">
      <_name>openSUSE Tumbleweed (Kubic DVD/USB Stick)</_name>
    </variant>

    <devices>
      <device id="http://usb.org/usb/80ee/0021"/> <!-- USB tablet -->
      <device id="http://pcisig.com/pci/8086/2415"/> <!-- AC97 -->
      <device id="http://pcisig.com/pci/1b36/0100"/> <!-- QXL -->
      <device id="http://pcisig.com/pci/1af4/1000"/> <!-- virtio-net -->
      <device id="http://pcisig.com/pci/1af4/1001"/> <!-- virtio-block -->
      <device id="http://pcisig.com/pci/1af4/1002"/>
      <device id="http://pcisig.com/pci/1af4/1003"/>
      <device id="http://pcisig.com/pci/1af4/1004"/>
      <device id="http://pcisig.com/pci/1af4/1005"/>
      <device id="http://pcisig.com/pci/1af4/1009"/>
      <device id="http://pcisig.com/pci/1af4/1041"/>
      <device id="http://pcisig.com/pci/1af4/1042"/>
      <device id="http://pcisig.com/pci/1af4/1043"/>
      <device id="http://pcisig.com/pci/1af4/1044"/>
      <device id="http://pcisig.com/pci/1af4/1045"/>
      <device id="http://pcisig.com/pci/1af4/1048"/>
      <device id="http://pcisig.com/pci/1af4/1049"/>
      <device id="http://pcisig.com/pci/1af4/1052"/>
      <device id="http://pcisig.com/pci/1af4/1050"/>
    </devices>

    <!-- DVD Image -->
    <media arch="i686">
      <variant id="dvd"/>
      <url>http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-DVD-i586-Current.iso</url>
      <iso>
        <volume-id>openSUSE-Tumbleweed-DVD-i586*</volume-id>
        <system-id>LINUX</system-id>
        <publisher-id>SUSE LINUX GmbH</publisher-id>
      </iso>
      <kernel>boot/i386/loader/linux</kernel>
      <initrd>boot/i386/loader/initrd</initrd>
    </media>
    <media arch="x86_64">
      <variant id="dvd"/>
      <url>http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-DVD-x86_64-Current.iso</url>
      <iso>
        <volume-id>openSUSE-Tumbleweed-DVD-x86_64*</volume-id>
        <system-id>LINUX</system-id>
        <publisher-id>SUSE LINUX GmbH</publisher-id>
      </iso>
      <kernel>boot/x86_64/loader/linux</kernel>
      <initrd>boot/x86_64/loader/initrd</initrd>
    </media>

    <!-- Network Image -->
    <media arch="i686">
      <variant id="network"/>
      <url>http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-NET-i586-Current.iso</url>
      <iso>
        <volume-id>openSUSE-Tumbleweed-NET-i586*</volume-id>
        <system-id>LINUX</system-id>
        <publisher-id>SUSE LINUX GmbH</publisher-id>
      </iso>
      <kernel>boot/i386/loader/linux</kernel>
      <initrd>boot/i386/loader/initrd</initrd>
    </media>
    <media arch="x86_64">
      <variant id="network"/>
      <url>http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-NET-x86_64-Current.iso</url>
      <iso>
        <volume-id>openSUSE-Tumbleweed-NET-x86_64*</volume-id>
        <system-id>LINUX</system-id>
        <publisher-id>SUSE LINUX GmbH</publisher-id>
      </iso>
      <kernel>boot/x86_64/loader/linux</kernel>
      <initrd>boot/x86_64/loader/initrd</initrd>
    </media>

    <!-- Kubic -->
    <media arch="x86_64">
      <variant id="kubic"/>
      <url>http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-Kubic-DVD-x86_64-Current.iso</url>
      <iso>
        <volume-id>openSUSE-Tumbleweed-Kubic-*</volume-id>
        <system-id>LINUX</system-id>
        <publisher-id>SUSE LINUX GmbH</publisher-id>
      </iso>
      <kernel>boot/x86_64/loader/linux</kernel>
      <initrd>boot/x86_64/loader/initrd</initrd>
    </media>

    <resources arch="all">
    <!-- http://en.opensuse.org/Hardware_requirements -->
      <minimum>
        <cpu>1600000000</cpu>
        <n-cpus>1</n-cpus>
        <ram>1073741824</ram>
        <storage>4294967296</storage>
      </minimum>
      <recommended>
        <cpu>2400000000</cpu>
        <ram>2147483648</ram>
        <storage>21474836480</storage>
      </recommended>
    </resources>

    <installer>
      <script id='http://opensuse.org/opensuse/autoyast/jeos'/>
      <script id='http://opensuse.org/opensuse/autoyast/desktop'/>
    </installer>
  </os>
</libosinfo>
